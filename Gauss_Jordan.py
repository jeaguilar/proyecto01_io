

class Gauss_Jordan:

    def buscar_columna_pivot(self, fila0, metodo, max):

        pivot = -1
        col_pivot = 0

        for i in range(0,len(fila0)-1):
            if metodo == 1:
                num = fila0[i][0]*3000 + (fila0[i][1])      # sustituye la m por un numero grande y evalua la ecuacion
            else:
                num = fila0[i]

            if (i == 0):                                # si es el primero
                pivot = num                             # el va ser el menor
                col_pivot = i                           # guarda la posicion

            if max:
                if (num < pivot):                       # si no compara contra el menor actual
                    pivot = num
                    col_pivot = i
            else:
                if (num > pivot):                       # si no compara contra el mayor actual
                    pivot = num
                    col_pivot = i

        return col_pivot


    def buscar_fila_pivot(self, columna_pivot, num_rest, matriz):

        largo = len(matriz[0])

        menor = -9696
        fila_pivot = 0                                  # posicion de la fila pivot

        acotada = 0
        degenerada = False

        for i in range(0,num_rest):
            elemento = matriz[i+1][columna_pivot]             # saca el elemento de la columna pivot
            resultado = matriz[i+1][largo-1]                  # saca el respectivo resultado a ese pivot (misma fila)

            if( elemento > 0 ):                                 # debe ser positivo
                division = resultado/elemento                   # realiza la division para encntrar el menor
                if (menor == -9696):                               # si es el primero de la lista va ser el menor
                    menor = division
                    fila_pivot = i+1
                    degenerada = False
                elif (division == menor):
                    degenerada = True

                elif( division < menor):                        # si no compara contra el menor actual
                    menor = division
                    fila_pivot = i+1                           # + 1 para ignora la fila 0 la de la U
                    degenerada = False                      # en caso de que el menor no sea problema de la degenerada

            else:
                acotada += 1

        if (acotada == num_rest):
            return -1                                  # -1 significa que es acotada


        return fila_pivot, degenerada


    def gauss_jordan(self, num_fila, fila, num_fila_pivot, num_columna_pivot, fila_pivot, metodo):

        fila_nueva = []                                         # fila nueva a retornar

        if num_fila == num_fila_pivot:
            num_pivot = fila[num_columna_pivot]
            for num in fila:
                nuevo_num = num / num_pivot
                fila_nueva += [nuevo_num]

        elif (num_fila == 0 and metodo == 1):
            num1_a_0 = fila[num_columna_pivot][0] * -1
            num2_a_0 = fila[num_columna_pivot][1] * -1

            for i in range(0, len(fila)):
                nuevo_num1 = (num1_a_0 * fila_pivot[i]) + fila[i][0]
                nuevo_num2 = (num2_a_0 * fila_pivot[i]) + fila[i][1]
                fila_nueva += [(nuevo_num1,nuevo_num2)]

        else:
            num_a_0 = fila[num_columna_pivot] * -1

            for i in range(0, len(fila)):
                nuevo_num = num_a_0 * fila_pivot[i] + fila[i]
                fila_nueva += [nuevo_num]

        return fila_nueva
