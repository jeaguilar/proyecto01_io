
import Gauss_Jordan as GJ
import Crear_Archivo as CA

class dos_fases():
	fila_var = []			#Fila con las variables de restriccion
	prev_fila_var = []		#Fila var de la primera fase
	columna_var = ["U "]	#Columna con las variables agregadas
	matriz_f1 = []			#Matriz para la fase 1
	matriz_f2 = []			#Matriz para la fase 2
	columna_pivot = 0		#Columna pivot actual
	fila_pivot = 0			#Fila pivot actual
	degenerada = False 		#Solucion degenerada
	nombre_archivo = ""		#Archivo por escribir
	num_r = 0				#Num de variables artificiales
	num_fase = 1			#Indica en que fase se encuentra
	es_max_tmp = False 		#Max temporal en la primera fase donde es minimizacion

	def __init__(self, archivo, num_var, num_rest, matriz, es_max):
		self.fila_var = []  # Fila con las variables de restriccion
		self.prev_fila_var = []  # Fila var de la primera fase
		self.columna_var = ["U "]  # Columna con las variables agregadas
		self.matriz_f1 = []  # Matriz para la fase 1
		self.matriz_f2 = []  # Matriz para la fase 2
		self.columna_pivot = 0  # Columna pivot actual
		self.fila_pivot = 0  # Fila pivot actual
		self.degenerada = False  # Solucion degenerada
		self.nombre_archivo = ""  # Archivo por escribir
		self.num_r = 0  # Num de variables artificiales
		self.num_fase = 1  # Indica en que fase se encuentra
		self.es_max_tmp = False  # Max temporal en la primera fase donde es minimizacion
		self.num_variables = num_var
		self.num_restricciones = num_rest
		self.matriz = matriz
		self.es_max = es_max
		self.nombre_archivo = archivo + "_sol.txt"
		self.realiza_fases()


	#Funcion Main
	def realiza_fases(self):
		self.es_max_tmp = self.es_max 	#Guarda el valor de es_max
		self.es_max = False 			#Minimizacion para la fase 1
		self.agrega_variables()
		self.crea_tabla()
		self.prepara_f1()
		self.operaciones_fila()
		self.crea_matriz_f2()
		self.prepara_f2()
		self.operaciones_fila()
		self.calcular_BF()

	#Agrega las variables de Holgura y artificiales
	def agrega_variables(self):		
		s = []		#Arreglo para guardar las variables de holgura
					#Agrega las variables basicas
		for i in range(0, self.num_variables): 
			self.fila_var += ["X"+str(i+1)]

					#Agrega las variables de holgura y artificiales
		for j in range(1, len(self.matriz)):
			len_fila = len(self.matriz[j])

			if self.matriz[j][len_fila-2] == "<=": #+S 
				s += ["S"+str(j)]
				self.columna_var += ["S"+str(j)] 

			elif self.matriz[j][len_fila-2] == ">=": #+R y -S
				self.fila_var += ["R"+str(j)]
				self.columna_var += ["R"+str(j)]
				s += ["S"+str(j)]
				self.num_r += 1
				
			elif self.matriz[j][len_fila-2] == "=": #+R
				self.fila_var += ["R"+str(j)]
				self.columna_var += ["R"+str(j)]
				self.num_r += 1
	
		self.fila_var += s 			#Agrega al final las de holgura para que este ordenado
		self.fila_var += ["Total"]	#Agrega la columna de total al final	


	#Imprime en pantalla por mientras
	def print_tabla(self):
		print(self.fila_var)
		for i in range(0,len(self.columna_var)):
			print(self.columna_var[i])


	#Crea la tabla para comenzar a iterar
	def crea_tabla(self):
		for i in range(0,len(self.matriz)):
			n_fila = []										#Nuevas filas para la matriz
			for j in range(0, len(self.fila_var)):

				if self.fila_var[j][0] == "R" and i == 0: 	#Llena la funcion U
					n_fila += [-1]
				
				elif i == 0:								#Llena la U de ceros
					n_fila += [0]

				elif j < self.num_variables: 				#Si es un numero se agrega normal
					n_fila += [self.matriz[i][j]]

				elif self.fila_var[j] == "R"+str(i): 		#Agrega R
					n_fila += [1] 

				elif self.fila_var[j] == "S"+str(i): 		#Agrega S
					if self.matriz[i][len(self.matriz[i])-2] == ">=":
						n_fila += [-1]
					else:
						n_fila += [1]

				elif j == len(self.fila_var)-1: 			#Agrega el resultado, ultimo de la fila
					n_fila += [self.matriz[i][-1]] 

				else:
					n_fila += [0]
				
			self.matriz_f1 += [n_fila] 						#Agrega fila a la nueva matriz
				

	#Convierte en 0's las variables artificiales de la Fila 0
	def prepara_f1(self):
		con_r = 1																	  #Lleva el orden de la variable R
		for i in range(self.num_variables,self.num_variables+self.num_restricciones): #Rango de variables agregadas
			if self.matriz_f1[0][i] == -1:											  #Variable artificial en la fila 0
				for j in range(0,len(self.matriz_f1[0])):							  #Moverse en la fila 0
					self.matriz_f1[0][j] += self.matriz_f1[con_r][j]				  #Suma de la fila 0
				con_r+=1

	
	#Convierte en 0's las variables que se encuentran en columna_var
	#Actualiza el fila_valor para la matriz de fase 2
	def prepara_f2(self):
		n_fila_var = []
		self.prev_fila_var = self.fila_var
		for i in range(1,len(self.columna_var)):		#Recorre primero la columna de variables
			for j in range(0,len(self.matriz_f2[0])):
				if self.fila_var[j] == self.columna_var[i]:
					numero_a_0 = self.matriz_f2[0][j]
					for k in range(0,len(self.matriz_f2[0])):
						self.matriz_f2[0][k] += (numero_a_0*-1) * self.matriz_f2[i][k]
		
		#Actualiza el valor de fila_var
		for i in range(0,len(self.matriz_f2[0])):
			n_fila_var += [self.fila_var[i]]
		self.fila_var = n_fila_var 

		
			
	#Crea la matriz para la segunda fase
	def crea_matriz_f2(self):
		for i in range(0,self.num_restricciones+1):
			n_fila = []
			for j in range(0,self.num_variables):
				if i == 0:
					n_fila += [self.matriz[i][j]*-1]
				else:
					n_fila += [self.matriz_f1[i][j]]
			
			n_fila += [self.matriz_f1[i][-1]]		#Agrega el resultado de la fase anterior
			self.matriz_f2 += [n_fila]				#Agrega la nueva fila a la matriz
		self.num_fase += 1

	def operaciones_fila(self):
		gauss = GJ.Gauss_Jordan()		#Se crea objeto Gauss Jordan
		archivo = CA.Crear_Archivo()	#Se crea objeto archivo
		terminar = 0					#Bandera para terminar la fase
		matriz = self.matriz_f1			#Inicia con la matriz de fase 1	
		estado = 0						#Numero de estado en que se trabaja
		num_bf = []

		if self.num_fase == 1:
			archivo.escribir(self.nombre_archivo, matriz, self.fila_var,self.columna_var,"inicial",0,0,0,[])
		else:
			archivo.escribir(self.nombre_archivo, matriz, self.fila_var, self.columna_var, "fase 2", 0, 0, 0, [])

		if self.num_fase == 2:
			matriz = self.matriz_f2 	#Cambia de matriz para la fase 2
			num_bf = self.calcular_BF() #Calcula el BF para la segunda fase
			self.es_max = self.es_max_tmp
			sol_multiple = self.solucion_mult(num_bf)
			pos_resultado = len(self.matriz_f2[0]) - 1
			print("U: " + str(self.matriz_f2[0][pos_resultado]))
			print("BF: " + str(num_bf))

			for i in num_bf:
				if i < 0:
					archivo.escribir(self.nombre_archivo, [], [], [], "no factible", 0, 0, 0, [])

				if sol_multiple != -1:
					self.fila_pivot, self.degenerada = gauss.buscar_fila_pivot(sol_multiple, self.num_rest, self.matriz_f2)
					self.columna_pivot = sol_multiple
					self.calcular_Gauss(gauss, archivo, "Solucion Multiple")
					bf = self.calcular_BF()
					archivo.escribir(self.nombre_archivo, self.matriz_f2, self.fila_var, self.columna_var, "final", 0, 0, 0, bf)
					pos_resultado = len(self.matriz_f2[0])-1
					print("U: " + str(self.matriz_f2[0][pos_resultado]))
					print("BF: " + str(bf))


		while terminar == 0:
			self.columna_pivot = gauss.buscar_columna_pivot(matriz[0],2,self.es_max) #Fase 1 es minimizacion
			self.fila_pivot, self.degenerada = gauss.buscar_fila_pivot(self.columna_pivot, self.num_restricciones, matriz)
			if self.fila_pivot == -1:
				break
			self.gauss_jordan(gauss, archivo, estado, matriz)
			estado += 1
			if self.fila_pivot == -1:
				archivo.escribir(self.nombre_archivo, matriz, self.fila_var,self.columna_var,"no acotada",0,0,0,[])
			else:
				archivo.escribir(self.nombre_archivo, matriz, self.fila_var,self.columna_var,"final",0,0,0,num_bf)
			if self.finalizar_fase():
				terminar += 1

	def finalizar_fase(self):
		matriz = self.matriz_f1
		if self.num_fase == 2:
			matriz = self.matriz_f2

		for i in range(0,len(matriz[0])-1):
			if self.es_max:					#En caso de maximizacion
				if matriz[0][i] < 0:
					return False
			else:
				if matriz[0][i] > 0:
					return False
		return True		

	def solucion_mult(self, num_bf):
		fila0 = self.matriz_f2[0]
		for i in range(0, len(num_bf)):
			if(fila0[i]== 0.0 and num_bf[i]==0):
				return  i
		return -1

	def calcular_BF(self):
	    lista_BF = []
	    pos_resultado = len(self.matriz_f2[0]) - 1

	    for i in range(0, pos_resultado):
	        lista_BF += [0]

	    for i in range(0, len(self.columna_var)):
	        if self.columna_var[i][0] == "X":
	            pos = int(self.columna_var[i][1:]) - 1       # extraerle el numero a la 'x2'  para en esa pos poner el resultado
	            lista_BF[pos] = self.matriz_f2[i][pos_resultado]

	    return lista_BF


	def gauss_jordan(self, gauss, archivo, estado, matriz):
		matriz = self.matriz_f1
		if self.num_fase == 2:
			matriz = self.matriz_f2

		columna_var_ant = self.columna_var[self.fila_pivot]			#Columna de variables pasadas
		self.columna_var[self.fila_pivot] = self.fila_var[self.columna_pivot]

		numero_pivot = matriz[self.fila_pivot][self.columna_pivot]

		n_fila = gauss.gauss_jordan(self.fila_pivot,matriz[self.fila_pivot],self.fila_pivot,self.columna_pivot,matriz[self.fila_pivot],2)
		matriz[self.fila_pivot] = n_fila
			
		for j in range(0, len(self.columna_var)): 
			if j != self.fila_pivot:
				n_fila = gauss.gauss_jordan(j,matriz[j],self.fila_pivot,self.columna_pivot,matriz[self.fila_pivot],2)
				matriz[j] = n_fila

		if self.degenerada:
			archivo.escribir(self.nombre_archivo,matriz,self.fila_var,self.columna_var,"degenerada",columna_var_ant,self.columna_var[self.fila_pivot],numero_pivot,[])
		else:
			archivo.escribir(self.nombre_archivo,matriz,self.fila_var,self.columna_var,str(estado),columna_var_ant,self.columna_var[self.fila_pivot],numero_pivot,[])
