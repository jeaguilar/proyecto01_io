
#Para probar el programa, deben de poner la direccion correcta del archivo de prueba
#pueden averiguar la direccion exacta en Pycharm haciendo clic izquierdo en el proyecto y seleccionar copy path

'''Funcion que lee el archivo de entrada de los diferentes problemas
su principal valor de retorno es una matriz, donde la primer columna 
es la funcion objetivo (U) y las demas columnas son sus restricciones
con la desigualdad o igualdad correspondientes.
'''
class Lector:
    def leer_archivo(self, nombre_archivo):

        signos = ["<=", ">=","="]
        archivo =open(nombre_archivo)
        lineasArchivo = [line.rstrip('\n') for line in archivo]

        #en las siguientes lineas se extraen los datos del problema
        datosProblema = lineasArchivo[0].split(',')
        metodo = int(datosProblema[0])
        optimizacion = datosProblema[1]
        num_variables_decision = int(datosProblema[2])
        num_restricciones = int(datosProblema[3])

        #recorta los datos del problema para que solo queden la objetivo y las restricciones
        lineasArchivo = lineasArchivo[1:]
        resultado_final_parcial =[]
        cont =0
        while cont < len(lineasArchivo):
            resultado_final_parcial.append(lineasArchivo[cont].split(','))
            cont = cont + 1

        resultado_final = []
        for elem in resultado_final_parcial:
            lista = []
            for sub_elem in elem:
                if sub_elem in signos:
                    lista += [sub_elem]
                else:
                    lista += [int(sub_elem)]
            resultado_final += [lista]

        archivo.close()



        return resultado_final,metodo,optimizacion,num_variables_decision,num_restricciones















