import Gauss_Jordan as GJ
import Crear_Archivo as CA

class metodo_simplex:

    degenerada = False

    nombre_archivo = ""


    '''Metodo donde se lleva a cabo la logica principal del programa utilizando las
    funciones auxiliares'''

    def metodo_main(self, problema, num_variables_decision, num_restricciones,optimizacion,nombre_archivo):
        self.nombre_archivo = nombre_archivo+"_sol.txt"

        #primero le quito los operadores a la matriz
        problema_sin_operadores = self.preparar_problema(problema,num_variables_decision)
        #creo la tabla simplex
        tabla_simplex = self.crear_tabla(problema_sin_operadores,num_variables_decision,num_restricciones)

        #creo la columna de variables basicas y la fila de variables basicas
        fila_VB,columna_VB = self.crear_variables_basicas(num_variables_decision,num_restricciones)

        #instancio la clase Gauss Jordan
        gauss = GJ.Gauss_Jordan()



        #inicializo las variables de columna y fila pivot
        columna_pivot = 0
        fila_pivot = 0

        archivo  = CA.Crear_Archivo()


        #validacion de terminacion
        terminar = self.termino_GJ(tabla_simplex, optimizacion)


        archivo.escribir(self.nombre_archivo,tabla_simplex,fila_VB,columna_VB,"inicial",0,0,0,[])
        estado = 0


        #Bucle donde se itera para llegar a la solucion de simplex, termina cuando la funcion de terminar seniala fin
        while(terminar==False):

            #calculo la columna pivot
            columna_pivot = gauss.buscar_columna_pivot(tabla_simplex[0],0,optimizacion)
            #calculo la fila pivot
            fila_pivot, self.degenerada = gauss.buscar_fila_pivot(columna_pivot,num_restricciones,tabla_simplex)

            if(fila_pivot==-1):
                break

            self.calcular_gauss_jordan(gauss,archivo,estado,columna_VB,fila_VB,fila_pivot,columna_pivot,tabla_simplex)
            #se actualiza el numero de iteracion del programa
            estado+=1
            #se calcula de nuevo la condicion de terminacion
            terminar = self.termino_GJ(tabla_simplex,optimizacion)


        #se valida que es no acotada
        if(fila_pivot == -1):
            archivo.escribir(self.nombre_archivo,tabla_simplex,fila_VB,columna_VB,"no acotada",0,0,0,[])
        else:
            #calcula el BF
            BF = self.calcular_bf(fila_VB, columna_VB, tabla_simplex)


            archivo.escribir(self.nombre_archivo, tabla_simplex, fila_VB, columna_VB, "final", 0, 0, 0, BF)
            #revisa si el problema tiene solucion multiple
            solucion_muliple = self.calcular_solucion_multiple(BF,tabla_simplex)
            for i in BF:
                if i < 0:
                    archivo.escribir(self.nombre_archivo, [], [], [], "no factible", 0, 0, 0, [])
            #entra al if si tiene solucion multiple
            if(solucion_muliple!=-1):
                columna_pivot = solucion_muliple
                fila_pivot, self.degenerada = gauss.buscar_fila_pivot(columna_pivot, num_restricciones, tabla_simplex)
                self.calcular_gauss_jordan(gauss, archivo, "Solucion Multiple", columna_VB, fila_VB, fila_pivot, columna_pivot,
                                           tabla_simplex)
                BF = self.calcular_bf(fila_VB, columna_VB, tabla_simplex)
                archivo.escribir(self.nombre_archivo, tabla_simplex, fila_VB, columna_VB, "final", 0, 0, 0, BF)

                for i in BF:
                    if i < 0:
                        archivo.escribir(self.nombre_archivo, [], [], [], "no factible", 0, 0, 0, [])


    def calcular_solucion_multiple(self,BF,tabla_simplex):

        fila0 = tabla_simplex[0]
        # si es las dos posiciones hay 0 quire decir que esa columna no esta dentro de las Variables Basicas
        for i in range(0, len(BF)):
            if (fila0[i] == 0 and BF[i] == 0):
                return i

        return -1



    def calcular_gauss_jordan(self,gauss,archivo,estado,columna_VB,fila_VB,fila_pivot,columna_pivot,tabla_simplex):

        columna_vieja = columna_VB[fila_pivot]

        # cambio el elemento de la columna de variables basicas por el respectivo en la fila de variables basicas
        columna_VB[fila_pivot] = fila_VB[columna_pivot]

        numero_pivot = tabla_simplex[fila_pivot][columna_pivot]

        nueva_fila = gauss.gauss_jordan(fila_pivot, tabla_simplex[fila_pivot], fila_pivot, columna_pivot,
                                        tabla_simplex[fila_pivot], 0)
        tabla_simplex[fila_pivot] = nueva_fila

        for i in range(0, len(columna_VB)):
            if (i != fila_pivot):
                nueva_fila = gauss.gauss_jordan(i, tabla_simplex[i], fila_pivot, columna_pivot,
                                                tabla_simplex[fila_pivot], 0)
                tabla_simplex[i] = nueva_fila

        '''
        print(fila_VB)
        print("Nueva tabla -----------")
        for fila in tabla_simplex:
            print(fila)
        print(columna_VB)
        '''

        #valida si la solucion es degenerada con la variable retornada en gauss jordan
        if self.degenerada:
            archivo.escribir(self.nombre_archivo, tabla_simplex, fila_VB, columna_VB, "degenerada", columna_vieja,
                             columna_VB[fila_pivot], numero_pivot, [])
        else:
            archivo.escribir(self.nombre_archivo, tabla_simplex, fila_VB, columna_VB, str(estado), columna_vieja,
                             columna_VB[fila_pivot], numero_pivot, [])


    def calcular_bf(self,fila_vb,columna_vb, tabla_simplex):

            largo_fila_vb = len(fila_vb)
            #quito el elemento de res en la fila vb
            fila_vb_sin_res = fila_vb[:largo_fila_vb-1]
            #quito el elemento de U en la columna Vb
            columna_vb_sin_u = columna_vb[1:]
            # lleno el BF de 0 para inicializarlo
            BF = [0] * (largo_fila_vb-1)

            lista_indices_modificados = []

            #guardo en una lista los indices de las X que fueron modificados
            for k in range(0,len(columna_vb_sin_u)):
                #guardo la VB en la posicion K
                string = columna_vb_sin_u[k]
                #remuevo la X
                string = string[1]
                #convierto en entero el indice
                indice = int(string)
                #guardo en la lista de indices el indice
                lista_indices_modificados += [indice]

            cont = 1
            #meto en la lista BF los resultados correspondientes con su indice
            for j in range(1,len(BF)+1):
                if(j in lista_indices_modificados):
                    BF[j-1] = tabla_simplex[cont][len(fila_vb)-1]
                    cont=cont+1

            return BF



    '''Condicion de parada para el metodo simplex: termina en maximizacion cuando los elementos de la fila 0 
    no contienen negativos, y en minimizacion cuando no contienen positivos'''
    def termino_GJ(self,matriz,max):
        largo = len(matriz[0])

        for i in range(0, largo-1):
            #pregunta la optimizacion deseada
            if max:
                if(matriz[0][i] < 0):
                    #si hay negativos, retorne false indicando que no ha acabado
                    return False
            else:

                if(matriz[0][i] > 0):
                    # si hay positivos, retorne false indicando que no ha acabado
                    return False

        return True

    '''Funcion que crea la lista de variables basicas tanto de fila como la columna'''
    def crear_variables_basicas(self,numero_variables_basicas, num_restricciones):

        fila_VB = []
        columna_VB = ["U "]

        largo = numero_variables_basicas+num_restricciones

        #se cuenta del largo del numero de variables basicas con el numero de restricciones
        for i in range(0,largo):
            fila_VB += ["X"+str(i+1)]
        #a columnas de variables basicas, le asigno las que estan en la fila de variables basicas usando strig slicing
        #con el string slicing
        columna_VB += fila_VB[numero_variables_basicas:]
        fila_VB += ["Resultado"]


        return fila_VB,columna_VB




    """Funcion que crea la tabla simplex, recibe la matriz lista, el numero de variables de decision y el 
    numero de restricciones"""
    def crear_tabla(self,matriz, num_variables_decision, num_restricciones):
        #variable donde se guarda el resultado
        res = []
        #la cantidad de columnas de la tabla, debe ser la cantidad de restricciones + las variables de decision
        #mas 1 para guardar el resultado
        num_columnas = num_restricciones+num_variables_decision+1
        num_filas = num_restricciones+1

        i = 0


        while i < len(matriz):
            j = 0
            while j < len(matriz[i]):
                cont = 0
                lista = []
                while cont <= num_columnas-1:
                    if cont < num_variables_decision:
                        lista += [matriz[i][j]]
                        j += 1
                        cont+=1

                    elif cont == num_columnas-1:

                        lista += [matriz[i][j]]
                        cont += 1
                    elif cont >= num_variables_decision and cont < num_columnas-1:

                        lista += [0]
                        cont += 1
                res += [lista]
                j +=1
            i +=1

        fila = 1
        while fila <= num_restricciones:
            res[fila][num_variables_decision+(fila-1)] += 1
            fila+=1

        return res




    '''funcion que toma el problema leido del archivo, convierte en negativos los elementos en la 
    funcion objetivo, remueve los signos de comparacion'''
    def preparar_problema(self,matriz, num_variables_decision):
        signos = ["<=", ">=", "="]
        problema_sin_operadores = []
        #se recorren los elementos de la matriz buscando signos
        for elem in matriz:
            lista = []
            for sub_elem in elem:
                #si es un signo, se ignora
                if sub_elem in signos:
                    pass
                #si no es un signo, se inserta en la fila que ira en la matriz
                else:
                    lista += [sub_elem]
            problema_sin_operadores += [lista]

        cont = 0

        #convierto en negativo los elementos en la U
        while cont < num_variables_decision:
            problema_sin_operadores[0][cont] = problema_sin_operadores[0][cont] * -1
            cont+=1

        #agrego el elemento 0 de la respuesta
        problema_sin_operadores[0] += [0]

        return problema_sin_operadores

