import copy
from fractions import Fraction

class Crear_Archivo:

    matriz = []
    metodo = 0

    tamanos = []

    def __init__(self):
        self.tamanos = []
        self.matriz = []

    ''' busca para cada una de las columnas el mayor numero de caracteres '''
    def buscar_largos(self):
        self.tamanos = []
        mayor = 0
        for i in range(0 , len(self.matriz[0])):
            for fila in self.matriz:
                num = fila[i]
                if (len(num) > mayor):
                    mayor = len(num)

            self.tamanos += [mayor]             # agrega a lista el mayor (numero de caracteres) de la columna 0, 1, .... n
            mayor = 0

    def num_a_faccion(self, numero):

        frac = Fraction(numero).limit_denominator(10)
        numerador = frac.numerator  # se le saca el numerador a la fracccion
        denominador = frac.denominator  # se le saca el numerador a la fracccion

        if numerador != 0:
            num_final = str(numerador)
            if denominador != 1:  # si el denominador es 1 no ocupa ser fraccion
                num_final = num_final + "/" + str(denominador)
        else:
            num_final = "0"

        return num_final



    def convertir_franccion(self):

        for fila in range(0,len(self.matriz)):
            for num in range(0, len(self.matriz[fila])):
                if type(self.matriz[fila][num]) is tuple:       # si es tupla es que el metodo es Gran M

                    num1 = self.matriz[fila][num][0]            # numero con M de la tupla[0]
                    num2 = self.matriz[fila][num][1]            # numero sin m de la tupla[1]

                    frac1 =  Fraction(num1).limit_denominator(10)   # saca la fracccion del numero con M
                    numerador1 = frac1.numerator               # se le saca el numerador a la fracccion del numero sin M
                    denominador1 = frac1.denominator           # se le saca el denominador a la fracccion del numero con M

                    frac2 = Fraction(num2).limit_denominator(10)    # saca la fracccion del numero sin M
                    numerador2 = frac2.numerator                # se le saca el numerador a la fracccion del numero sin M
                    denominador2 = frac2.denominator            # se le saca el denominador a la fracccion del numero sin M

                    num_final1 = ""
                    num_final2 = ""

                    if numerador1 != 0:
                        num_final1 = str(numerador1)
                        if denominador1 != 1:                   # si el denominador es 1 no ocupa ser fraccion
                            num_final1 = num_final1 + "/" + str(denominador1) +"M"
                        else:
                            num_final1 = num_final1 +"M"        # solamente se la agrega la M para el caso de que sea el numero con M

                    if numerador2 != 0:
                        if (numerador2 < 0 or numerador1 == 0):
                            num_final2 = str(numerador2)
                        else:
                            num_final2 = "+" + str(numerador2)

                        if denominador2 != 1:
                            num_final2 = num_final2 + "/" + str(denominador2)

                    if numerador1 == 0 and numerador2 == 0:     # si los dos son 0 se pone unicamente un 0
                        num_final1 = "0"

                    num_final = num_final1 + num_final2

                else:
                    numero = self.matriz[fila][num]             # si no es tupla es un numero normal
                    num_final = self.num_a_faccion(numero)

                self.matriz[fila][num] = num_final


    def escribir_matriz(self, archivo, fila_VB, columna_VB):
        archivo.write("|VB" + "  ")

        for i in range(0, len(fila_VB)):                    # escribir todas las variables basicas de arriba
            archivo.write("|" + fila_VB[i])                 # escribe en el doc el string
            limite = self.tamanos[i] - len(fila_VB[i]) + 2  # el limite es la diferencia entre el mas largo de su columna mas dos para que se vea mas separado
            for j in range(0, limite):                      # esto para agregarle los espacios respectivos
                archivo.write(" ")

        archivo.write("\n")                                 # escribe un cambio de linea

        for fila in range(0, len(self.matriz)):
            archivo.write("|" + columna_VB[
                fila] + "  ")                               # se a escribir para cada una de las filas las variables basicas de la columna de primero

            for numero in range(0, len(self.matriz[0])):                # se agarra cada numero de las fila respectiva
                archivo.write("|" + self.matriz[fila][numero])          # escribe el numero en el doc
                limite = self.tamanos[numero] - len(self.matriz[fila][
                                                        numero]) + 2    # el limite es la diferencia entre el mas largo de su columna mas dos para que se vea mas separado

                for j in range(0, limite):                  # esto para agregarle los espacios respectivos
                    archivo.write(" ")

            archivo.write("\n")


    def escribir(self, nombre_archivo ,matriz, fila_VB, columna_VB, estado, col_saliente, col_entrante, numero_pivot, b_f):
        # el estado es un numero de por cual iteracion de la matriz va, tambien se puede mandar incial y final
        # col saliente es cual variable de la columna de variables basicas va cambiar por la nueva que es pivot
        # col entrante es la variable de la columna pivot

        self.matriz = copy.deepcopy(matriz)
        self.convertir_franccion()
        self.buscar_largos()

        archivo = open(nombre_archivo, "a")

        if estado == "inicial":
            archivo.close()
            archivo = open(nombre_archivo, "w")
            archivo.write("Estado " + estado + "\n\n")

            self.escribir_matriz(archivo, fila_VB, columna_VB)
            archivo.write("\n\n")

        elif estado == "fase 2":
            archivo.write("Estado " + estado + "\n\n")

            self.escribir_matriz(archivo, fila_VB, columna_VB)
            archivo.write("\n\n")

        elif estado == "no acotada":
            archivo.write("Funcion es no acotada")

        elif estado == "no factible":
            archivo.write("La solución es no factible")

        elif estado == "final":
            largo = len((self.matriz[0]))
            archivo.write("Respuesta final ->  U: " + self.matriz[0][largo-1])
            archivo.write("  BF: (")
            for i in range(0,len(b_f)):
                num_fraccion = self.num_a_faccion(b_f[i])
                archivo.write(str(num_fraccion))
                if i < len(b_f)-1:
                    archivo.write(",")

            archivo.write(")")
            archivo.write("\n\n")


        else:
            archivo.write("Estado " + estado +"\n\n")

            self.escribir_matriz(archivo,fila_VB,columna_VB)

            num_fraccion = self.num_a_faccion(numero_pivot)

            archivo.write("\n")
            archivo.write("VB entrante : " + col_entrante)
            archivo.write(",  VB saliente: " + col_saliente)
            archivo.write(",  Número Pivot: " + str(num_fraccion))

            archivo.write("\n\n")


        archivo.close()

