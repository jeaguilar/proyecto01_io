from fractions import Fraction
import Gauss_Jordan as GJ
import Crear_Archivo as CA

class Gran_M():

    nombre_archivo = ""
    
    num_variables = 0
    num_rest = 0

    funcionU = []
    lista_restric = []

    max = True

    num_r = 0
    num_s = 0

    r_usadas = 0
    s_usadas = 0

    tabla = []

    fila_VB = []
    col_VB = ["U "]

    columna_pivot = 0
    fila_pivot = 0

    degenerada = False

    def __init__(self, archivo, num_var, num_restricciones, matriz, es_max):
        self.tabla = []
        self.fila_VB = []
        self.col_VB = ["U "]
        self.r_usadas = 0
        self.s_usadas = 0
        self.columna_pivot = 0
        self.fila_pivot = 0
        self.num_variables = num_var
        self.num_rest = num_restricciones
        self.funcionU = matriz[0]
        self.lista_restric = self.formar_restricciones(matriz[1:])
        self.max = es_max
        self.nombre_archivo = archivo + "_sol.txt"

    def formar_restricciones(self,restric):

        matriz = []
        for i in range(0, self.num_rest):                   # [num_variables, 1 = R , 1 o -1 = S, resultado]
            lista = []
            for j in range(0, self.num_variables + 2):      # +2 por el operador y el resultado
                if (j == self.num_variables):               # valida si está en el operador
                    operador = restric[i][j]
                    if(operador == "="):
                        lista += [1]                        # agrega un 1 de que hay una r
                        self.num_r += 1                     # suma el contador de cuantas r hay variable para la hora de crear la matriz
                        lista += [0]                        # 0 para saber que no tiene s
                        self.col_VB += ["R" + str(self.num_r)]  # para este caso solo la R es importante como variable basica
                    elif(operador == ">="):
                        lista += [1]                        # agrega un 1 de que hay una r
                        self.num_r += 1                     # suma el contador de cuantas r hay
                        lista += [-1]                        # agrega un 1 de que hay una s
                        self.num_s += 1                     # suma el contador de cuantas s hay
                        self.col_VB += ["R" + str(self.num_r)]  # para este caso solo la R es importante como variable basica
                    else:
                        lista += [0]                        # 0 para saber que no tiene r
                        lista += [1]                        # agrega un 1 de que hay una s
                        self.num_s += 1                     # suma el contador de cuantas s hay
                        self.col_VB += ["S" + str(self.num_s)]  # para este caso como no hay R es importante como  guardar la S con su restectivo indice
                else:
                    lista += [restric[i][j]]
            matriz += [lista]

        return matriz


    def despejar_R(self):

        rest_despejada = []
        for cont in range(0,(self.num_variables+1)):                # rellenar la lista con 0 para en cada pos ir sumando de una vez los x1 con x1 y asi respectivamente
            rest_despejada += [0]

        for i in range(0,self.num_rest):                                        # recorrer cada una de las restricciones
            rest = self.lista_restric[i]
            if (rest[self.num_variables] == 1):                                 # verificar si tiene varaibles R para saber si se usa o se ignora
                limit = self.num_variables + 3                                  # 3 = 1 campo para la R + 1 campo para la S + 1 campo para resultado
                for j in range(0, limit):
                    if (self.max):                                              # si se maximiza
                        if (j == (limit - 1)):                                  # solamnete el resultado no se le cambia el signo
                            rest_despejada[self.num_variables] += rest[j]       # el num_variables es porque son arreglos de tamaños diferentes

                        elif (j != self.num_variables and j != (self.num_variables + 1)):  # ignorar los campos de R y S
                            n = rest[j] * -1                                    #  cambiar de signo
                            rest_despejada[j] += n                              # sumarselo al que esta en esa pos
                    else:
                        if (j == (limit - 1)):                                  # solamente al resultado se le cambia el signo
                            n = rest[j] * -1                                    # cambio de signo
                            rest_despejada[self.num_variables] += n             # sumarlo  al que esta en esa pos

                        elif (j != self.num_variables and j != (self.num_variables + 1)):   # ignorar los campos de R y S
                            rest_despejada[j] += rest[j]
        return rest_despejada


    def despejar_U(self):

        for i in range(0, len(self.funcionU)):          # cambiar de signo por el despeje a 0 de la U
            self.funcionU[i] = self.funcionU[i] * -1

        lista_M = self.despejar_R()                     # despeja las restricciones con R

        u_despejada = []
        largo = len(self.funcionU)+1

        for i in range(0,largo):
            if (i == largo-1):                          # solamente al result se le cambia el signo por el despeje
                result = lista_M[i] * -1                # cambio de signo
                tupla = (result,0)                      # crea la tupla donde el primero es el que lleva la variable M segundo numero normal
            else:
                tupla = (lista_M[i],self.funcionU[i])   # crea la tupla donde el primero es el que lleva la variable M segundo numero normal

            u_despejada += [tupla]                      # agrega la tupla a la lista

        return u_despejada


    def meter_en_matriz(self, pos, lista):

        tam = self.num_variables +1
        largo_lista = len(lista)
        for i in range(0,tam):
            if(i == tam-1):
                ultimo = lista[largo_lista-1]           # agarra el ultimo elemento de la lista que es el resultado
                self.tabla[pos] += [ultimo]             # se lo concatena para que este de ultimo en la lista respectiva
            else:
                self.tabla[pos][i] = lista[i]           # agrega cada elemento a la lista


    def crear_matriz(self):

        largo = self.num_variables + self.num_r + self.num_s    # genera un largo para las columnas sin el resultado, ya que luego va ser concatenado
        for i in range(0,self.num_rest+1):
            lista = []
            for j in range(0,largo):                            # rellena la lista de 0
                if (i == 0):
                    lista += [(0,0)]
                else:
                    lista += [0]
            self.tabla += [lista]                               # agrega la lista a la tabla

        u = self.despejar_U()                                   # crea la u despejada con M's
        self.meter_en_matriz(0, u)                              # mete la u despejada a la tabla
        for i in range(0, self.num_rest):                       # para agarrar cada restrccion
            self.meter_en_matriz(i + 1, self.lista_restric[i])  # envia a agregar cada restriccion a la tabla


    def agregar_R_S(self, fila):

        if(fila == 0):
            for i in range(0,self.num_s):
                pos_s = self.num_variables + self.num_r                 # pos donde comienzan las S's
                if (self.max):
                    self.tabla[fila][pos_s+i] = (1,0)                   # si es max las S's quedan positivas
                else:
                    self.tabla[fila][pos_s + i] = (-1,0)                # si es min las S's quedan en negativo
        else:
            pos_r = self.num_variables + self.r_usadas                  # pos de donde tengo que poner su R
            pos_s = (self.num_variables + self.num_r ) + self.s_usadas  # pos de donde tengo que poner su S

            if( self.num_r > 0):                                        # si hay R's
                self.tabla[fila][pos_r] = self.lista_restric[fila-1][self.num_variables]
            if (self.num_s > 0):                                        # si hay S's
                self.tabla[fila][pos_s] = self.lista_restric[fila-1][self.num_variables+1]

            if (self.lista_restric[fila-1][self.num_variables] != 0):       # si en esa pos hay algo diferente de 0
                self.r_usadas += 1                                          # tiene una R
            if (self.lista_restric[fila-1][self.num_variables+1] != 0):     # si en esa pos hay algo diferente de 0
                self.s_usadas += 1                                          # tiene una S


    def crear_fila_VB(self):

        for i in range(0, self.num_variables):              # por cada numero de variables se le agrega un Xn
            var = "x" + str(i+1)
            self.fila_VB += [var]

        for i in range(0, self.num_r):                      # por cada restrccion que tenga R se le agrega Rn
            var = "R" + str(i+1)
            self.fila_VB += [var]

        for i in range(0, self.num_s):                      # por cada restriccion que tenga S se le agrega Sn
            var = "S" + str(i+1)
            self.fila_VB += [var]

        self.fila_VB += ["LD"]


    def termino_GJ(self):

        l = len(self.tabla[0]) - 1
        fila0 = self.tabla[0][:l]
        for elemt1 , elemt2 in fila0:                       # agarra cada elemento de la fila 0
            num = elemt1*3000 + elemt2                      # sustituye la M por un numero grande
            if self.max:                                    # si es max busca que todos sean positivos o 0
                if (num < 0):
                    return False                            # aun hay negativos no termina
            else:                                           # min busca que todos sean negativos o 0
                if (num > 0):
                    return False                            # aun hay positivos no termina
        return True


    def solucion_multiple(self, bf):

        fila0 = self.tabla[0]
                                    # si es las dos posiciones hay 0 quire decir que esa columna no esta dentro de las Variables Basicas
        for i in range(0,len(bf)):
            if (fila0[i] == (0.0,0.0) and bf[i] == 0):              # comparo con tupla por ser la Gran M
                return i

        return -1



    def calcular_BF(self):
        lista_BF = []
        pos_resultado = len(self.tabla[0]) - 1

        for i in range(0, pos_resultado):
            lista_BF += [0]

        for i in range(0, len(self.col_VB)):
            if self.col_VB[i][0] == "x":
                pos = int(self.col_VB[i][1:]) - 1       # extraerle el numero a la 'x2'  para en esa pos poner el resultado
                lista_BF[pos] = self.tabla[i][pos_resultado]

            elif self.col_VB[i][0] == "R":              # extraerle el numero a la 'R2' y sumarle la cantidad de variables
                pos = int(self.col_VB[i][1:]) + self.num_variables - 1      #  para en esa pos poner el resultado
                lista_BF[pos] = self.tabla[i][pos_resultado]

            elif self.col_VB[i][0] == "S":              # extraerle el numero a la 'R2' y sumarle la cantidad de variables
                pos = int(self.col_VB[i][1:]) + self.num_variables + self.num_r - 1  #  para en esa pos poner el resultado
                lista_BF[pos] = self.tabla[i][pos_resultado]

        return lista_BF


    def calcular_Gauss(self, gauss, archivo, estado):


        col_VB_vieja = self.col_VB[self.fila_pivot]
        self.col_VB[self.fila_pivot] = self.fila_VB[self.columna_pivot]

        numero_pivot = self.tabla[self.fila_pivot][self.columna_pivot]

        nueva_fila = gauss.gauss_jordan(self.fila_pivot, self.tabla[self.fila_pivot], self.fila_pivot,
                                        self.columna_pivot, self.tabla[self.fila_pivot], 1)         # manda a opera la fila pivot

        self.tabla[self.fila_pivot] = nueva_fila

        for i in range(0, len(self.col_VB)):
            if (i != self.fila_pivot):
                nueva_fila = gauss.gauss_jordan(i, self.tabla[i], self.fila_pivot, self.columna_pivot,
                                                self.tabla[self.fila_pivot], 1)                     # manda a opera la filas con la fila pivot
                self.tabla[i] = nueva_fila

        if self.degenerada:                             # verifica si es degenerada y lo escribe el archivo
            archivo.escribir(self.nombre_archivo, self.tabla, self.fila_VB, self.col_VB, "degenerada", col_VB_vieja,
                             self.col_VB[self.fila_pivot], numero_pivot, [0, 0])
        else:
            archivo.escribir(self.nombre_archivo, self.tabla, self.fila_VB, self.col_VB, str(estado), col_VB_vieja,
                             self.col_VB[self.fila_pivot], numero_pivot, [0, 0])



    def calcular_gan_M(self):

        self.crear_matriz()                             # crea la tabla sin R's ni S's

        for i in range(0,self.num_rest+1):              # para cada restriccion voy a agregarle su respectiva R y S
            self.agregar_R_S(i)

        self.crear_fila_VB()                            # crea la fila con la variables utilizadas X's, R's y S's

        gauss = GJ.Gauss_Jordan()                       # inicializa las clase
        archivo = CA.Crear_Archivo()                    # inicializa la clase

        terminar = self.termino_GJ()                    # verifica si ya termino si llegó a la ultima iteracion
        estado = 0                                      # contador de numero de iteracion para la matriz

        archivo.escribir(self.nombre_archivo,self.tabla, self.fila_VB, self.col_VB,"inicial", 0, 0, 0, [0,0])   # escribe la matriz inicial para comenzar a operala


        while (terminar == False):

            self.columna_pivot = gauss.buscar_columna_pivot(self.tabla[0],1,self.max)       # busca la columna pivot
            self.fila_pivot, self.degenerada = gauss.buscar_fila_pivot(self.columna_pivot,self.num_rest, self.tabla)    # busca la fila pivot

            if self.fila_pivot == -1:           # si es -1 quire decir que es acotada
                break

            self.calcular_Gauss(gauss,archivo,estado)   # manda a opera la matriz operaciones fila

            estado += 1
            terminar = self.termino_GJ()        # verifica si ya llego a la matriz final

        if self.fila_pivot == -1:       # si es no acotada la funcion
            archivo.escribir(self.nombre_archivo, self.tabla, self.fila_VB, self.col_VB, "no acotada", 0, 0, 0, [0, 0])
        else:
            bf = self.calcular_BF()      # calcula el BF
            archivo.escribir(self.nombre_archivo, self.tabla, self.fila_VB, self.col_VB, "final", 0, 0, 0, bf)

            sol_multiple = self.solucion_multiple(bf)   # verifica si tiene solucion multiple
            pos_resultado = len(self.tabla[0])-1        # posicion donde se encuantran los resultados es las filas respectivas

            print("U: " +  str(self.tabla[0][pos_resultado][1]))
            print("BF: " + str(bf))
            print("\n\n")

            for i in bf:                            # verifica si cada uno de los elementos del bf son positivos
                if i < 0:                           # si hay negativos es no factible
                    archivo.escribir(self.nombre_archivo, [], [], [], "no factible", 0, 0, 0, [])

            if(sol_multiple != -1):     # sol_multiple seria la posicion de la columna en 0, que seria la pivot si es -1 es que no hay sol multiple

                self.fila_pivot, self.degenerada = gauss.buscar_fila_pivot(sol_multiple, self.num_rest, self.tabla)
                self.columna_pivot = sol_multiple

                self.calcular_Gauss(gauss, archivo, "Solucion Multiple")

                bf = self.calcular_BF()
                archivo.escribir(self.nombre_archivo, self.tabla, self.fila_VB, self.col_VB, "final", 0, 0, 0, bf)
                print("U: " + str(self.tabla[0][pos_resultado][1]))
                print("BF: " + str(bf))

                for i in bf:                        # verifica si cada uno de los elementos del bf son positivos
                    if i < 0:
                        archivo.escribir(self.nombre_archivo, [], [], [], "no factible", 0, 0, 0, [])

























