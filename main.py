import Gran_M as GM
import Lector as Archivo
import Dual
import dos_fases as DF

import simplex_metodo as SM
import sys

if __name__ == '__main__':

    lector = Archivo.Lector()

    for i in range(1,len(sys.argv)):

        try:
            matriz_inicial, metodo, optimizacion, num_variables_decision, num_restricciones = lector.leer_archivo(str(sys.argv[i]))

            tam_nombre = len(str(sys.argv[i])) - 4  # el menos 4 es para eliminar el .txt)

            nombre_archivo = str(sys.argv[i])[:tam_nombre]

            if (metodo == 0):
                obj_simplex = SM.metodo_simplex()
                if (optimizacion == "max"):
                    obj_simplex.metodo_main(matriz_inicial, num_variables_decision, num_restricciones, True,nombre_archivo)
                else:
                    obj_simplex.metodo_main(matriz_inicial, num_variables_decision, num_restricciones, False,nombre_archivo)

            elif (metodo == 1):
                if (optimizacion == "max"):
                    obj_metodo = GM.Gran_M(nombre_archivo, num_variables_decision, num_restricciones, matriz_inicial, True)
                else:
                    obj_metodo = GM.Gran_M(nombre_archivo, num_variables_decision, num_restricciones, matriz_inicial, False)

                obj_metodo.calcular_gan_M()

            elif (metodo == 2):
                if (optimizacion == "max"):
                    Dos_Fases = DF.dos_fases(nombre_archivo, num_variables_decision, num_restricciones, matriz_inicial,
                                             True)
                else:
                    Dos_Fases = DF.dos_fases(nombre_archivo, num_variables_decision, num_restricciones, matriz_inicial,
                                             False)

            elif (metodo == 3):
                if (optimizacion == "max"):
                    Dual.Dual(num_variables_decision, num_restricciones, matriz_inicial, True,nombre_archivo)
                else:
                    Dual.Dual(num_variables_decision, num_restricciones, matriz_inicial, True,nombre_archivo)

            else:
                print("El método no existe")

        except:
            print("Fallo del archivo: " + sys.argv[i])

