import simplex_metodo
import numpy as np
import Gauss_Jordan as GJ
import Crear_Archivo as CA

class Dual():
    num_variables = 0
    num_restricciones = 0
    es_max = True
    num_variables_agregadas = 0
    matriz = []
    matriz_dual = []
    der_restricciones = []
    der_restricciones = []

    fila_literal = []
    columna_literal = []

    fila_pivote = -1
    menor = 999999
    columna_pivote = -1

    _POS_FUNCION = 0

    holguras = []
    nombre_archivo = ""

    degenerada = False


    def __init__(self, num_var, num_rest, matriz, es_max,nombre_archivo):

        self.num_variables = 0
        self.num_restricciones = 0
        self.es_max = True
        self.num_variables_agregadas = 0
        self.matriz = []
        self.matriz_dual = []
        self.der_restricciones = []
        self.der_restricciones = []

        self.fila_literal = []
        self.columna_literal = []

        self.fila_pivote = -1
        self.menor = 999999
        self.columna_pivote = -1

        self.holguras = []
        self.nombre_archivo = ""

        self.degenerada = False

        self.num_variables = num_var
        self.num_variables = num_var
        self.num_restricciones = num_rest
        self.matriz = matriz
        self.es_max = es_max
        self.nombre_archivo = nombre_archivo + "_sol.txt"

        self.calcular_dual()

    def calcular_dual(self):

        self._tomar_der_restricciones()
        self._generar_matriz_dual()
        self._despejar_funcion()
        self._preparar_restricciones()

        self.num_variables, self.num_restricciones = self.num_restricciones, self.num_variables #Luego de pasar de primal a dual, la cantidad de restricciones pasa a ser la cantidad de variables basicas, y viceversa
        self.es_max = not self.es_max #Si es max, una vez pasado a dual, se vuelve min

        self._cambiar_holguras_negativas()

        self._agregar_ceros_funcion()
        self._agregar_ceros_restricciones()

        self._generar_fila_literal()
        self._generar_columna_literal()
        #self.print_matriz()

        gauss = GJ.Gauss_Jordan()
        archivo = CA.Crear_Archivo()
        estado = 0

        nueva_fila =[]
        i = 0

        self._seleccionar_fila_pivote(self.matriz_dual)
        self._seleccionar_columna_pivote(self.matriz_dual)

        archivo.escribir(self.nombre_archivo,self.matriz_dual,self.fila_literal,self.columna_literal,"inicial",0,0,0,[])

        while self._ha_terminado() == False:
            self.fila_pivote = -1
            self.columna_pivote = -1
            self.menor = 99999

            self._seleccionar_fila_pivote(self.matriz_dual)
            self._seleccionar_columna_pivote(self.matriz_dual)

            if self.fila_pivote == -1:
                break;
            self._calcular_gauss(gauss,archivo,estado)
            estado +=1

        if self.fila_pivote == -1:
            archivo.escribir(self.nombre_archivo, self.matriz_dual, self.fila_literal, self.columna_literal, "no acotada",0, 0, 0, [])
        else:
            bf = self._calcular_bf()
            archivo.escribir(self.nombre_archivo, self.matriz_dual, self.fila_literal, self.columna_literal, "final",0, 0, 0, bf)
            sol_multiple = self._solucion_multiple(bf)
            for i in bf:
                if i < 0:
                    archivo.escribir(self.nombre_archivo, [], [], [], "no factible", 0, 0, 0, [])
            if(sol_multiple != -1):
                self.columna_pivote = sol_multiple
                self._seleccionar_fila_pivote(self.matriz_dual)
                self._calcular_gauss(gauss,archivo,"Solucion Multiple")
                bf = self._calcular_bf()
                archivo.escribir(self.nombre_archivo, self.matriz_dual, self.fila_literal, self.columna_literal, "final", 0, 0, 0, bf)
                for i in bf:
                    if i < 0:
                        archivo.escribir(self.nombre_archivo, [], [], [], "no factible", 0, 0, 0, [])

        self._imprimir_final(bf)
        #self.print_matriz()
        #self._dar_respuesta_PRIMAL()
        #self._dar_respuesta_DUAL()

    def _tomar_der_restricciones(self):

        for i in range(1, self.num_restricciones+1):
            largo_res = len(self.matriz[i])
            self.der_restricciones.append(self.matriz[i][largo_res-1])

    def _generar_matriz_dual(self):
        self.matriz_dual.append(self.der_restricciones) #Se agrega la lista que contenia el lado derecho de las desigualdades
        self._transpuesta_restricciones()
        self._agregar_signos()

    def _transpuesta_restricciones(self):
        submatriz = []
        w = 0
        for i in range(1, len(self.matriz)):
            submatriz.append([])
            for j in range(0, len(self.matriz[i]) - 2):
                submatriz[w].append(self.matriz[i][j])
            w += 1

        submatriz = np.transpose(submatriz)

        self._reasignar_restricciones(submatriz)

    def _reasignar_restricciones(self, submatriz):
        for i in range(0, self.num_variables):
            self.matriz_dual.append([])
            for j in range(0, self.num_restricciones):
                self.matriz_dual[i+1].append(submatriz[i][j])


    def _agregar_signos(self):
        contador = 1
        w = 0
        for i in range(1, self.num_variables + 1):
            w = i

            if contador == len(self.matriz):
                break
            largo_res = len(self.matriz[i])
            signo = self.matriz[i][largo_res - 2]
            if signo == "<=" or signo == "<" :
                signo = ">="  # Se cambia el signo
            else:
                signo = "<="  # Se cambia el signo
            contador += 1
            self.matriz_dual[i].append(signo)
            self.matriz_dual[i].append(self.matriz[0][i - 1])  # Se colocan los coeficientes como nuevos valores de en las restricciones

        while contador < len(self.matriz_dual):
            self.matriz_dual[contador].append(signo)
            self.matriz_dual[contador].append(
            self.matriz[0][w - 1])  # Se colocan los coeficientes como nuevos valores de en las restricciones
            w + 1
            contador += 1



    def _preparar_restricciones(self):
        '''
        Se eliminan las desigualdades de las restricciones, agregando las variables de holgura
        '''

        #Se inicia en 1 para saltar la lista que contiene la función
        for i in range(1, len(self.matriz_dual)):
            largo_ecuacion = len(self.matriz_dual[i])

            #el signo de <= siempre va a estar en el penultimo lugar
            if(self.matriz_dual[i][largo_ecuacion-2] == "<="):

                # si es mayor o igual se debe de restar una variable de holgura
                #se elimina el simbolo de esa ecuacion, dando a entender que ya es una igualdad
                self._agregar_holgura(self.matriz_dual[i], largo_ecuacion, True)
            else:
                # si es menor, se agrega una variable positiva para sumar lo que hace falta
                self._agregar_holgura(self.matriz_dual[i], largo_ecuacion, False)

    def _agregar_holgura(self, ecuacion, largo, positiva):
        if positiva:
            # se cambia el signo por un 1 que representa la variable de holgura positiva, y se pone en la posicion del signo que tenia, dando a entender que ahora es una igualdad
            ecuacion[largo-2] = 1
        else:
            ecuacion[largo - 2] = -1

    def _despejar_funcion(self):
        '''
        Se iguala la funcion objetivo a 0
        '''

        for i in range(0, len(self.matriz_dual[self._POS_FUNCION])):
            #Se cambian los valores de los coeficientes de la funcion objetivo, simulando que se pasaron de lado
            self.matriz_dual[self._POS_FUNCION][i] *= -1

        #self.matriz_dual[self._POS_FUNCION][:0] = [1] #Se agrega 1 simulando la Z
        self.matriz_dual[self._POS_FUNCION].append(0) #Se agrega un 0 al final, el resultado de despejar la funcion objetivo

    def _cambiar_holguras_negativas(self):
        for i in range(1, len(self.matriz)-1):
            largo = len(self.matriz_dual[1])
            if self.matriz_dual[i][largo-2] == -1:
                self._negar_lista(self.matriz_dual[i])

    def _negar_lista(self,lista):
        for i in range(0, len(lista)):
            lista[i] *= -1

    def _agregar_ceros_funcion(self):
        for i in range(0, self.num_restricciones):
            self.matriz_dual[self._POS_FUNCION].append(0) #Se agrega un 0 por cada var de holgura que se haya agregado

    def _agregar_ceros_restricciones(self):
        for i in range(1, self.num_restricciones+1):
            self._agregar_ceros_izq(i,i-1)
            self._agregar_ceros_der(i,self.num_restricciones -i)

    def _generar_fila_literal(self):

        for i in range(1,self.num_variables+1):
            self.fila_literal.append("X"+str(i))
        for i in range(1, self.num_restricciones+1):
            self.fila_literal.append("S"+str(i))
        self.fila_literal.append("R")

    def _generar_columna_literal(self):
        self.columna_literal.append("U ")
        for i in range(1,self.num_restricciones+1):
            self.columna_literal.append("S"+str(i))

    def _agregar_ceros_izq(self,pos,cantidad):
        largo = len(self.matriz_dual[pos])
        self.matriz_dual[pos] = self.matriz_dual[pos][:largo-2] + [0]*cantidad + self.matriz_dual[pos][largo-2:]

    def _agregar_ceros_der(self,pos, cantidad):
        largo = len(self.matriz_dual[pos])
        self.matriz_dual[pos] = self.matriz_dual[pos][:largo - 1] + [0] * cantidad + self.matriz_dual[pos][largo - 1:]

    def print_matriz(self):
        print("\t",end="")
        for i in range(0, len(self.fila_literal)):
            print(self.fila_literal[i]," | ", end="")
        for i in range(0, len(self.matriz_dual)):
            print()
            print(self.columna_literal[i], "\t",end="")
            for j in range(0, len(self.matriz_dual[i])):
                print(self.matriz_dual[i][j]," | ", end="")
        print("\n")

    def _seleccionar_fila_pivote(self, matriz):
        for i in range(1, len(matriz)):
            largo = len(matriz[i])
            if(matriz[i][largo-1] < self.menor):
                self.menor = matriz[i][largo-1]
                self.fila_pivote = i

    def _seleccionar_columna_pivote(self,matriz):
        division_menor = 999999
        self.degenerada = False
        for i in range(0, len(self.matriz_dual)):
            if(matriz[0][i] != 0 and matriz[self.fila_pivote][i]):
                div = matriz[0][i] / matriz[self.fila_pivote][i]
                if div == division_menor:
                     self.degenerada = True
                if div < division_menor:
                    division_menor = div
                    self.columna_pivote = i

    def _ha_terminado(self):
        respuesta = False
        if self._respuestas_positivas() or self._U_positivas():
            respuesta = True
        return respuesta

    def _respuestas_positivas(self):
        respuesta = True
        for i in range(0, len(self.matriz_dual)):
            largo = len(self.matriz_dual[i])
            if self.matriz_dual[i][largo-1] < 0:
                respuesta = False
        return respuesta


    def _U_positivas(self):
        respuesta = True
        for i in range(0, len(self.matriz_dual[self._POS_FUNCION])):
            if self.matriz_dual[self._POS_FUNCION][i] < 0:
                respuesta = False
        return respuesta



    def _calcular_bf(self):
        lista_BF = []
        pos_resultado = len(self.matriz_dual[0]) - 1



        for i in range(0, pos_resultado):
            lista_BF += [0]

        for i in range(0, len(self.columna_literal)):
            if self.columna_literal[i][0] == "X":
                pos = int(self.columna_literal[i][1:]) - 1
                lista_BF[pos] = self.matriz_dual[i][pos_resultado]

            elif self.columna_literal[i][0] == "S":
                pos = int(self.columna_literal[i][1:]) + self.num_variables-1
                lista_BF[pos] = self.matriz_dual[i][pos_resultado]

        return lista_BF

    def _solucion_multiple(self, bf):

        fila0 = self.matriz_dual[self._POS_FUNCION]

        for i in range(0,len(bf)):
            if (fila0[i] == 0.0 and bf[i] == 0):
                return i

        return -1

    def _dar_respuesta_DUAL(self):
        print("La respuesta que soluciona el metodo DUAL son las siguientes")
        for i in range(0,len(self.matriz_dual)):
            largo = len(self.matriz_dual[i])
            print(self.columna_literal[i],": ",self.matriz_dual[i][largo-1])

    def _dar_respuesta_PRIMAL(self):
        print("La respuesta que soluciona el metodo PRIMAL son las siguientes")

        res = self.num_restricciones
        largo = len(self.matriz_dual[self._POS_FUNCION])-1

        for i in range(0, self.num_restricciones):

            respuesta = self.matriz_dual[self._POS_FUNCION][largo-res]
            res-=1
            if(respuesta < 0):
                respuesta *=-1

    def _imprimir_final(self,bf):
        largo = len(self.matriz_dual[self._POS_FUNCION]) - 1
        print("U: ", self.matriz_dual[self._POS_FUNCION][largo])
        print("BF: ", bf)

    def _calcular_gauss(self,gauss,archivo,estado):
        columna_anterior = self.columna_literal[self.fila_pivote]

        self.columna_literal[self.fila_pivote] = self.fila_literal[
            self.columna_pivote]  # Se cambia la variable de la fila pivote por la de la columna pivote

        num_pivote = self.matriz_dual[self.fila_pivote][self.columna_pivote]

        self.matriz_dual[self.fila_pivote] = gauss.gauss_jordan(self.fila_pivote, self.matriz_dual[self.fila_pivote],
                                                                self.fila_pivote, self.columna_pivote,
                                                                self.matriz_dual[self.fila_pivote], 3)

        for i in range(0, len(self.columna_literal)):
            if (i != self.fila_pivote):
                self.matriz_dual[i] = gauss.gauss_jordan(i, self.matriz_dual[i], self.fila_pivote, self.columna_pivote,
                                                         self.matriz_dual[self.fila_pivote], 3)

        if (self.degenerada):
            archivo.escribir(self.nombre_archivo, self.matriz_dual, self.fila_literal, self.columna_literal,
                             "degenerada", columna_anterior, self.columna_literal[self.fila_pivote], num_pivote, [])
        else:
            archivo.escribir(self.nombre_archivo, self.matriz_dual, self.fila_literal, self.columna_literal,
                             str(estado), columna_anterior, self.columna_literal[self.fila_pivote], num_pivote, [])



